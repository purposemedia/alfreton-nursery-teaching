<?php

use PM\Helpers;

$dates = transform_dates_to_array(wp_get_archives([
	'format' => 'custom',
	'echo' => 0
]));
$title = single_month_title(' ', false);

$data = [
	'pagination' => Timber::get_pagination(),
	'categories' => Timber::get_terms('category'),
	'dates' => $dates,
	'date' => $title,
	'title' => $title,
	'title_prefix' => 'News in: '
];

PM\Helpers::render($data, true, 'home.twig');