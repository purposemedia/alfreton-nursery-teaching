<?php

use Timber\Post;
use Timber\Helper;

add_action('after_setup_theme', function() {

	function register() {
		register_nav_menus([
			'secondary' => 'Secondary Menu',
			'mobile' => 'Mobile Menu',
			'terms' => 'Terms & Conditions Menu'
		]);
	}
	add_action('init', 'register');

	function get_latest() {
		return Timber::get_posts(array(
			'post_type' => 'post',
			'posts_per_page' => 2
		));
	}
	Helper::function_wrapper('get_latest');

	// Related posts in same category, if none then latest posts
	function related(Post $post, int $limit = 2, string $type = '') {
		$args = [
			'post_type' => ($type) ? $type : $post->type->name,
			'posts_per_page' => $limit,
			'orderby' => 'rand'
		];
		
		if($type === $post->type->name || empty($type)) {
			$args = array_merge($args, [
				'cat' => implode(',', array_column($post->categories(), 'id')),
				'post__not_in' => [$post->id]
			]);
		}    	
		
		return Timber::get_posts($args);
	}
	Helper::function_wrapper('related');

	function transform_dates_to_array(string $dates) {
		$re = '/<a href=[\'"](.*)[\'"]>(.*)<\/a>/i';
		preg_match_all($re, $dates, $results, PREG_SET_ORDER, 0);
		
		$arr = [];

		array_walk($results, function($date) use (&$arr) {
			$arr[] = [
				'link' => $date[1],
				'name' => $date[2]
			];
		});

		return $arr;
	}
	
}, 10);