<?php

use PM\Helpers;

$dates = transform_dates_to_array(wp_get_archives([
	'format' => 'custom',
	'echo' => 0
]));

$page = Timber::get_post(get_option('page_for_posts'));

PM\Helpers::render([
	'pagination' => Timber::get_pagination(),
	'categories' => Timber::get_terms('category'),
	'dates' => $dates,
	'title' => 'School News'
]);