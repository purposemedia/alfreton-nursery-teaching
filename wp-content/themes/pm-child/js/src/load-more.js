const $ = require('jquery');

module.exports = function loadMore(callbacks) {
	var $trigger = $('.js-load-more');
	var $container = $('.c-post-archive__inner, .c-news ');
	
	if(!$trigger.length) return;
	
	$trigger.click(function(e) {
		e.preventDefault();
		
		if(callbacks.start !== 'undefined') callbacks.start($trigger);
		
		$.get($(this).attr('href'), function(data) {
			var $data = $(data);
			var $link = $data.find('.js-load-more');
			if($link.length) {
				$trigger.attr('href', $link.attr('href'));
			} else {
				if(typeof callbacks.finished !== 'undefined') callbacks.finished($trigger);
			}
			
			$container.append($data.find('.c-post-archive__inner, .c-news').html());
			
			if(typeof callbacks.end !== 'undefined') callbacks.end($trigger, $container);
		});
	});
}