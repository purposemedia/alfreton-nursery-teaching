const loadMore = require('./load-more');
const $ = require('jquery');
require('jquery-match-height');

// var Turbolinks = require("turbolinks");
// Turbolinks.start();

$(document).ready(function() {
	loadMore({
		start: function($trigger) {
			$trigger.addClass('c-btn--disabled');
		},
		finished: function($trigger) {
			$trigger.parents('.c-post__pagination, .c-news__pagination').remove();
		},
		end: function($trigger, $container) {
			$trigger.removeClass('c-btn--disabled');
		}
	});

	$(".js-mobile-menu").click(function(){
		$(".c-mobile-nav").toggleClass('active');
	});

	$(".js-search").click(function(){
		$(".o-row--search").toggleClass('active');
	});

	$(".js-menu-icon").click(function(){
		$(".menu-icon").toggleClass('active');
		$(".c-mobile-nav").toggleClass('active');
	});

	$(".js-drop").click(function(){
		$(".mobile-menu-drop, .mobile-menu__item, .mobile-menu").toggleClass('active');
	});

	$(".c-staff__item").matchHeight();
});