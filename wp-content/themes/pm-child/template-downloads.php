<?php
/**
 * Template Name: Downloads
 */

use PM\Helpers;

if ( post_password_required( $post->ID ) ) {
	$data['passwordProtected'] = true;
	PM\Helpers::render($data, true, 'template-downloads-password.twig');
} else {
	PM\Helpers::render();
}