#PM WP

## What you need to know
- Use ```debug_log``` - you can leave debug info for the next developer and not worry about production environments
- Classes that handle activating/upgrading the theme are in ```src/```
- When the theme gets updated it will also run the activation methods
- When the theme gets activated, it will; install required plugins (and if there are new ones, install them too), create a child theme and activate it
- Upgrade specific methods can be created in ```src/VersionUpgrades.php```, create a new function that matches the new version *number* prefixed by a ```v```, e.g ```1.2.3``` -> ```public static function v123() {}```
- Pushing out updates are handled by kernl.us

##Unit Tests
- Coming soon, feel free to start

## Preliminary to pushing out updates
- Make sure you have thoroughly tested the theme in lots of scenarios, this update will go out to *every site that has the theme installed*
- Triple(!) check your new features and the general working of the theme
- Version numbers should be formatted ```{major}.{minor}.{patch}```. A major update resets the minor & patch number, a minor update resets the patch number. Examples:
	- If you are pushing a major update for ```1.2.12```, the new version is ```2.0.0```
	- If you are pushing a minor update for ```1.2.12```, the new version is ```1.3.0```
	- If you are pushing a patch update for ```1.2.12```, the new version is ```1.2.13```
- Pull down the BitBucket ```wordpress-base-theme``` repo

## Pushing out updates
- Make your changes and test them
- If it's a particulary large update, make sure you have created a version upgrade method if needed
- Change the version number in ```style.css```
- Change the version number in ```kernl.version``` - if you don't change this a build won't be pushed out
- Push your changes up to the BitBucket repo
- Kernl.us will build a new version and push it out to users