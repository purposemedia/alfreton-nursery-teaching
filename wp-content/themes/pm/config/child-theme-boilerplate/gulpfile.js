//packages
var browserify = require('browserify');
var babelify = require('babelify');

var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

//paths
var paths = {
	css: {
		watch: 'css/src/**/*.scss',
		dest: 'css/dist/'
	},
	js: {
		watch: 'js/src/**/*.js',
		src: 'js/src/master.js',
		dest: 'js/dist/',
		destFile: 'master.js'
	}
};

gulp.task('css', function() {
	return gulp.src(paths.css.watch)
		.pipe(sourcemaps.init())
			.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
			.pipe(autoprefixer())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.css.dest));
});

gulp.task('js', function(){
	var bundler = browserify(paths.js.src, {debug: true}).transform(babelify);

	bundler.bundle()
		.on('error', function(err) { console.error(err); this.emit('end'); })
		.pipe(source(paths.js.destFile))
		.pipe(buffer())
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(paths.js.dest));
});

gulp.task('watch', function() {
	gulp.watch(paths.js.watch, ['js']);
	gulp.watch(paths.css.watch, ['css']);
});

gulp.task('default', ['css', 'js']);