<?php

$autoloader = require __DIR__ . '/vendor/autoload.php';

use Timber\Timber;
use PM\Bootstrap;

if(!defined('PM_HEADLESS')) define('PM_HEADLESS', false);

if(!PM_HEADLESS) {
	new Timber();
	Timber::$dirname = 'views';
	
	$bootstrap = new Bootstrap;
	
	/**
	 * GLOBAL FUNCTIONS
	 */
	if(!function_exists('dd')) {
		function dd($val) { var_dump($val); die(); }
	}

	function add_ccf_nonces() {
		echo "<script>
			document.addEventListener('DOMContentLoaded', function () {
				var inputs = document.querySelectorAll('input[name=\'form_nonce\']');
				
				if(inputs.length) {
					var request = new XMLHttpRequest();
					request.addEventListener('error', function() { console.error('Can not retrieve new nonce token for ccf forms')});
					request.addEventListener('load', function() {
						if(request.status === 404 || request.status === 500) {
							console.error('Attempting to retrieve new nonce token for ccf forms returned 404/500 error');
						} else {
							for(var i = 0; i < inputs.length; i++) {
								inputs[i].setAttribute('value', request.response);
							}
						}
					});

					request.open('GET', '" .  get_template_directory_uri() . "/nonce.php');
					request.send(null);
				}
			});
		</script>";
	}
	add_action('wp_footer', 'add_ccf_nonces');
}