<?php

/**
 * This file will purge timber/twig cache, you need to setup a notification in deployhq
 */

define('WP_USE_THEMES', false);

require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/wp-load.php');

if($nonce = wp_create_nonce('ccf_form')) {
	echo $nonce;
} else {
	status_header(404);
}