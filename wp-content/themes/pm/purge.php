<?php

/**
 * This file will purge timber/twig cache, you need to setup a notification in deployhq
 */

require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/wp-load.php');

$loader = new TimberLoader();
$loader->clear_cache_timber();
$loader->clear_cache_twig();