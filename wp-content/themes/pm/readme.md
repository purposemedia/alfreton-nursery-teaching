# Requires

- PHP >=5.5
- (Recommended) For auto purging cache after deploying to servers you will need to do the following:
	- Go to the DeployHQ repo
	- Go to Settings -> Notifications
	- Add a new Notification
	- Leave Notification Type as ```HTTP Post```
	- Enter the endpoint as ```http://example.com/wp-content/themes/pm/purge.php``` and change the domain to the site you are setting this up for
	- Hit save, cache will be purged everytime you deploy.

# Installation
- Download the latest version from Downloads on the left
- Rename the un-zipped folder to ```pm``` and put it in your theme directory
- If you want to, you can run ```composer install``` in the directory to make sure everything is up to date.
- Activate the PM theme, this will install all plugins and the child theme, this may take a while, leave it running and make a tea.
- Run ```npm i``` in the child directory (by default it is ```pm-child```)
- Run ```bower install``` in the child directory (by default it is ```pm-child```)

# Features
- Uses Timber for templating (Twig engine). Caches templates out the box (does not cache for development environments)
- ~~WP will use `bcrypt` rather than `md5`. ~100 million times harder to crack. (http://security.stackexchange.com/a/61387)~~ **Working on this, pluggable functions aren't available in theme scope**
- Gulpfile is setup and ready to go, just use ```gulp watch``` (as long as ```npm i``` has ran)
- Automatically updates Wordpress core (major and minor)
- Automatically installs required plugins (including paid) on theme activation and theme update
- Automatically generates and activates child theme on parent theme activation
- Automatically sets urls on local (only) environments
- Turns on debug for local environments if not already configured (```wp-config.php``` can overwrite this)
- Disables file editing via admin
- Automatically registers ACF Options fields; contact details, 404 content etc
- Upgrade specific methods (E.g, upgrades to ```v1.0.1``` can check for known security vulnerabilities in a plugin and resolve it)

# Templating Helpers
- `{{ menu.MENUNAME.get_items }}` - gets items from whichever `MENUNAME` you specify (`menu_MENUNAME` is also a deprecated alias)
- `{{ breadcrumbs($before, $after) }}` - displays ```yoast_breadcrumb```
- `{{ website_link($attr) }}` - displays website link with `a` tag if on front page
- `{{ get_template_link($name) }}` - retrives link to the first page with the template specified

# Helpers
These helpers are namespaced, so make sure you add ```use PM\Helpers``` at the top of your files.

- ```Helpers::render(array, mixed(int,array,boolean), mixed(array,string))``` loads .twig template automatically without providing path to template, adheres to WP hierarchy (e.g ```index.php``` => ```views/index.twig```, ```archive-properties.php``` => ```views/archives/properties.twig```). The first parameter allows you to pass mixed data to the template. You can override the path with the third parameter (just provide ```views/my/custom/route``` or an array of strings), however you shouldn't need to. For more information on the second parameter (caching), please see https://jarednova.github.io/timber/#cache-the-entire-twig-file-and-data
- ```Helpers::cleanup(array(file path,stream,zip_archive,curl))``` runs garbage collections on a variable number of parameters
- ```Helpers::debug_log(mixed(string,object,array))``` logs data to ```wp-content/debug.log``` - use this instead of ```print_r/var_(dump/export)```
- ```Helpers::get_config(string)``` reads JSON config files in ```themes/pm/config/``` and returns an array of options

# Contributors
- Please read CONTRIBUTING.MD for details on how to deploy a new version

# Notes
- Yes, the vendor folder is in the repo, yes it's supposed to be there. We can't run commands on shared servers so this is the best backwards compatible solution (as long as we don't add too many packages). It also allows us to roll out updates of packages with the theme.