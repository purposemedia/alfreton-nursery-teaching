<?php

namespace PM;

use PM\Helpers;
use PM\VersionUpgrades;

//Autoloads these in construct
use PM\Register;
use PM\TimberExtras;
use PM\ThemeUpdateChecker;
use PM\ThemeActivationHandler;

class Bootstrap {
	private $paths;

	public function __construct() {
		add_action('init', array($this, 'setup_local'));
		add_action('init', array($this, 'setup_non_local'));
		add_action('admin_notices', array($this, 'add_debug_notice'));
		add_action('admin_notices', array($this, 'add_timber_notice'));

		$this->paths = Helpers::get_config('paths');

		$register = new Register;
		if(class_exists('Timber')) $extras = new TimberExtras;
		$activationHandler = new ThemeActivationHandler;

		$update = Helpers::get_config('update');
		$checker = new ThemeUpdateChecker($update['name'], $update['url']);
	}

	/**
	 * On localhost: Sets option URLS to URL you are on
	 * Will not do this on live to avoid duplicate content
	 *
	 * @return null
	 */
	public function setup_local() {
	    if(Helpers::is_development()) {
	        $currentDomain = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];

	        defined('WP_HOME') or define('WP_HOME', $currentDomain);
	        defined('WP_SITEURL') or define('WP_SITEURL', $currentDomain);
	    }
	}

	public function add_timber_notice() {
		if(!class_exists('Timber')) {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
		}
	}
	
	/**
	 * Notifies admin to enable debug mode if on development server
	 */
	public function add_debug_notice() {
	    if(Helpers::is_development() && (!WP_DEBUG || !WP_DEBUG_LOG)) {
	        $class = 'update-nag';
	        $message = 'Please set WP_DEBUG ' . (!WP_DEBUG_LOG ? 'and WP_DEBUG_LOG ' : '') . 'to true in wp-config.php';

	        echo '<div class="' . $class . '"><p>'  . $message . '</p></div>';
	    }
	}

	public function setup_non_local() {
		add_theme_support('post-formats');
		add_theme_support('post-thumbnails');
		add_theme_support('menus');

	    defined('DISALLOW_FILE_EDIT') or define('DISALLOW_FILE_EDIT', true);
	    defined('CORE_UPGRADE_SKIP_NEW_BUNDLED') or define('CORE_UPGRADE_SKIP_NEW_BUNDLED', true);
	    defined('WP_AUTO_UPDATE_CORE') or define('WP_AUTO_UPDATE_CORE', true);
	    
	    add_filter( 'automatic_updates_is_vcs_checkout', '__return_false' );
	    add_filter('upload_mimes', array($this, 'extended_mime_types'));
		$this->add_timber_locations();
		$this->post_upgrade();
	}

	/**
	 * Ensures Timber will always find the file
	 *
	 * TODO: Measure performance and make adjusments
	 * Possible create a hash of current dir structure, if it changes then update $locations?
	 */
	private function add_timber_locations() {
		if(!class_exists('Timber')) { Helpers::debug_log('Timber doesn\'t exist, not adding locations'); return; }

		$path = ABSPATH . 'wp-content/themes/' . $this->paths['child-theme'] . '/views';
		if(file_exists($path)) {
			$iterator = new \RecursiveIteratorIterator(
				new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS),
				\RecursiveIteratorIterator::SELF_FIRST
			);
			
			$additions = array();
			foreach($iterator as $file) {
				if($file->isDir()) $additions[] = $file->getRealpath();
			}
			
			\Timber::$locations = $additions;
		} else {
			Helpers::debug_log('Child theme doesn\'t exist, can\'t create locations');
		}
	}

	/**
	 * We call this here to ensure the new version is activated, runs upgrade methods
	 */
	private function post_upgrade() {
		if($versions = get_option('pm_upgrade_versions')) {
			$versions = json_decode($versions, true);
			$currentTheme = wp_get_theme($this->paths['parent-theme']);

			if($versions['new'] === $currentTheme->get('Version')) {
				$versions = array_map(function($v) { return 'v' . filter_var($v, FILTER_SANITIZE_NUMBER_INT); }, $versions);
				
				//TODO: this needs to use VersionUpgrades class with child classes, but for now uses basic function
				$vUpgrades = new VersionUpgrades($versions['old'], $versions['new']);
				$vUpgrades->run();

				$handler = new ThemeActivationHandler;
				$handler->install(true);
				
				delete_option('pm_upgrade_versions');
			}
		}
	}

	public function extended_mime_types($mime_types = array()) {
        return array_merge($mime_types, Helpers::get_config('mime-types')); 
	}
}