<?php
/**
 * If you want to add helpers to be used in .twig templates, please add them to TimberExtras.php
 */

namespace PM;

class Helpers {
    /**
     * Gets config data from json files in /config/
     * 
     * @param $name:string
     * @return array
     */
    public static function get_config($name, $parent = false) {
        //TODO: Memoization and refactor to class file handler
        $path = '/config/' . $name . '.json';
        
        if(!$parent) {
            $path = get_stylesheet_directory() . $path;
        } else {
            $path = get_template_directory() . $path;
        }
        
        if(file_exists($path)) {
            $handler = fopen($path, 'r');

            $data = json_decode(fread($handler, filesize($path)), true);

            fclose($handler);

            return $data;
        } else {
            if(!$parent) {
                $func = __FUNCTION__;
                return self::$func($name, true);
            } else {
                self::debug_log('get_config can\'t find path: ' . $path);
            }
        }
    }

    /**
     * Logs data to wp-content/debug.log
     *
     * @param $data:mixed(string|array|object)
     * @return null
     */
    public static function debug_log($data) {
        if(WP_DEBUG === true) {
            $dbg_trc = debug_backtrace();
            
            $pre = (isset($dbg_trc[1]['file'])) ? $dbg_trc[1]['file'] . ':' . $dbg_trc[1]['line'] . ' (' . $dbg_trc[1]['function'] . ') - ' : '';
            if(is_array($data) || is_object($data)) {
                error_log($pre . print_r($data, true));
            } else {
                error_log($pre . $data);
            }
        }
    }

    public static function is_development() {
        return in_array($_SERVER['SERVER_ADDR'], array('127.0.0.1', '::1')) || getenv('APP_ENV') === 'homestead' || (isset($_SERVER['USER']) && $_SERVER['USER'] === 'vagrant');
    }

    public static function cleanup($arguments) {
        foreach($arguments as $args) {
            $args = (is_object($args)) ? array(0 => $args) : (array) $args;
            $clean = function(){};
            
            if(is_string($args[0]) && file_exists($args[0])) $clean = function($val) { @unlink($val); };

            if(is_resource($args[0]) && get_resource_type($args[0]) === 'stream') $clean = function($val) { @fclose($val); };

            if(is_resource($args[0]) && get_resource_type($args[0]) === 'curl') $clean = function($val) { @curl_close($val); };

            array_walk($args, $clean);
        }
    }

    /**
     * Loads the twig template based on current filename, adheres to WP template hierarchy
     *
     * @param $data:mixed(array|object
     * @param $cache:mixed(boolean|integer)
     * @param $path:mixed(string|array) - Overrides the 'best guess' template path
     */
    public static function render($data = array(), $cache = true, $path = '') {
        if(!class_exists('Timber')) { self::debug_log('Timber is not activated, failing silently'); return; }

        //gets file name of file that requested this function
        $dbg_trc = debug_backtrace();
        $dbg_file = $dbg_trc[0];
        if(!isset($dbg_file['file'])) { self::debug_log('We aren\'t calling pm_render from a view file'); return; }
        
        $file = $dbg_file['file'];
        $fileParts = explode(DIRECTORY_SEPARATOR, $file);
        $fileName = end($fileParts);

        if(!$path) {
            $exceptions = array('single-' => 'singles', 'tag-' => 'tags', 'category-' => 'categories', 'taxonomy-' => 'taxonomies', 'archive-' => 'archives', 'page-' => 'pages');
            $interim = '/';
            $find = array('.php');
            $replace = array('.twig');

            foreach($exceptions as $exception => $folder) {
                if(strpos($fileName, $exception) === 0) {
                    $find[] = $exception;
                    $replace[] = '';
                    $interim .= $folder . DIRECTORY_SEPARATOR;
                    break;
                }
            }
            
            $path = 'views' . $interim . str_replace($find, $replace, $fileName);
        }
        
        $context = \Timber::get_context();
        array_walk($context, function($val, $key) use (&$data) { if(!isset($data[$key])) { $data[$key] = $val; } });

        if(!isset($data['post'])) {
            //TimberPost always returns something from what I can tell
            $data['post'] = new \TimberPost();
        }
        
        if(!is_array($path)) {
            $path = '/' . $path;
        }

        if(!self::is_development()) {
            //if true, set to an hour, if it's an int, set it to that value, otherwise turn cache off
            $cache = ($cache === true) ? 3600 : (is_int($cache)) ? $cache : false;
        } else {
            $cache = false;
        }
        
        return \Timber::render($path, $data, $cache);
    }
}