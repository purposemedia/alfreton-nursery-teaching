<?php

namespace PM;

use PM\Helpers;

class Register {
    public function __construct() {
        add_action('wp_enqueue_scripts', array($this, 'js'));
        add_action('wp_enqueue_scripts', array($this, 'css'));
        if(class_exists('acf')) add_action('init', array($this, 'acf'));
        
        $this->menus();
    }

    public function css() {
        wp_register_style('child-master', get_stylesheet_directory_uri() . $this->appendModified('/css/dist/master.css'));

        wp_enqueue_style('child-master');
    }

    public function js() {
        wp_register_script('child-master', get_stylesheet_directory_uri() . $this->appendModified('/js/dist/master.js'));

        wp_enqueue_script('child-master', false, array(), false, true);
    }

    private function menus() {
        register_nav_menus(array(
            'header' => 'Header',
            'footer' => 'Footer'
        ));
    }
    
    public function acf() {
        if(!get_field('acf_options')) {
            if(function_exists('acf_add_options_page')) acf_add_options_page();
            register_field_group(Helpers::get_config('custom-fields-options'));
        }
    }
    
    private function appendModified($file) {
        if(Helpers::is_development()) return $file; //crashes vagrant if we don't do this
        return $file . '?v=' . (filemtime(get_stylesheet_directory() . $file));
    }
}