<?php

namespace PM;

use PM\Helpers;

class ThemeActivationHandler {

	private $paths;

	public function __construct() {
		$pathConfig = Helpers::get_config('paths');

		$this->paths = array(
			'boilerplate' => get_template_directory() . '/config/child-theme-boilerplate',
			'child-dir' => ABSPATH . 'wp-content/themes/' . $pathConfig['child-theme']
		);

		add_action('after_switch_theme', array($this, 'install'));
		add_action('switch_theme', array($this, 'switchTheme'));
	}
	
	/**
	 * If the new theme is not the child theme
	 * then run the uninstall
	 *
	 * @return null
	 */
	public function switchTheme($newTheme) {
		if(get_option('pm_installed')) delete_option('pm_installed');
	}
	
	/**
	 * Downloads plugins, activates plugins, creates child theme
	 * then sets DB option to installed
	 *
	 * @return null
	 */
	public function install($override = false) {
		if($override || !get_option('pm_installed')) {
			$plugins = Helpers::get_config('plugins');
			
			$downloaded = array();
			$failed = array();
			foreach($plugins as $plugin) {
				if((isset($plugin['development']) && $plugin['development'] === 'true') && !Helpers::is_development()) continue;
				
				$url = (isset($plugin['url'])) ? $plugin['url'] : null;
				$dl = $this->downloadPlugin($plugin['name'], $url);
				
				if($dl !== 'exists') {
					$type = 'downloaded';
				} elseif($dl === 'exists') {
					continue;
				} else {
					$type = 'failed';
				}
				
				${$type}[] = $plugin['name'] . '/' . (isset($plugin['file']) ? $plugin['file'] : $plugin['name']) . '.php';
			}

			if(count($downloaded) > 0 || count($failed) > 0) {
				$this->activatePlugins($downloaded, $failed);
			}
			
			$this->createChildTheme();

			$this->activateChildTheme();

			add_option('pm_installed', true);
		}
	}

	/**
	 * Creates an empty zip, downloads plugin into the zip,
	 * extracts zip to the plugins directory
	 *
	 * @param $name:string
	 * 
	 * @return mixed(boolean)
	 */
	private function downloadPlugin($name, $url = null) {
		$path = ABSPATH . 'wp-content/plugins/' . $name;

		if(file_exists($path)) {
			return 'exists';
		} else {
			$path = $path . '.zip';

			if(!$newPlugin = fopen($path, 'wb')) {
				Helpers::debug_log('Can\'t create zip for ' . $newPlugin);
				return false;
			} else {
				//initalizing the zip archive here seems to be more consistent than after curl
				$zip = new \ZipArchive;

				/**
				 * TODO: Possibly a very minor security issue
				 * but only for the time the zip exists (< 10 sec)
				 * and only on updating/activation/deactivation of theme
				 */
				chmod($path, 0777);
			}

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_FILE, $newPlugin);
			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_URL, ($url) ?: 'http://downloads.wordpress.org/plugin/' . $name . '.zip');

			curl_exec($ch);

			if(curl_exec($ch) === false) {
				Helpers::debug_log('CURL failed on plugin download (' . $name . '), error code:' . curl_errno($ch));

				Helpers::cleanup(array($ch, $newPlugin, $path));
				return false;
			} else {
				if($zip->open($path, \ZipArchive::CREATE) === TRUE) {
					$zip->extractTo(str_replace($name . '.zip', '', $path));

					Helpers::cleanup(array($newPlugin, $path, $zip, $ch));
				} else {
					Helpers::debug_log($zip->getStatusString());
					Helpers::debug_log('Can\'t open zip for ' . $name);

					Helpers::cleanup(array($path, $newPlugin, $ch, $zip));
		         	return false;
				}
			}

			return true;
		}
	}

	/**
	 * Activates any of the plugins that have been sucesfully downloaded
	 * and are currently inactive, does not deactivate any other plugins
	 *
	 * @param $plugins:array
	 *
	 * @return null
	 */
	private function activatePlugins($plugins, $failed = array()) {
		$activated = get_option('active_plugins');

		if(gettype($activated) === 'array' && count($activated) > 0) {
			array_map(function($val) use (&$plugins, $failed) {
				if(array_search($val, $plugins) === false && array_search($val, $failed) === false) {
					array_push($plugins, $val);
				}
			}, $activated);
		} else {
			add_option('active_plugins', '');
		}

		foreach($plugins as $plugin) {
			do_action('active_' . $plugin);
			do_action('activated_plugin', $plugin);
		}

		update_option('active_plugins', $plugins);
	}

	/**
	 * Creates the child theme
	 *
	 * @return mixed(null|boolean)
	 */
	private function createChildTheme() {
		if(!file_exists($this->paths['child-dir'])) {
			if(mkdir($this->paths['child-dir'])) {
				if(file_exists($this->paths['boilerplate'])) {
					$this->moveFiles(scandir($this->paths['boilerplate']));
				} else {
					Helpers::debug_log('Boilerplate folder doesn\'t exist');
					return false;
				}
			} else {
				Helpers::debug_log('Can\'t create child theme directory');
				return false;
			}
		} else {
			//TODO: assume we don't need to retransfer boilerplate?
			Helpers::debug_log('Child theme already exists');
			return false;
		}
	}

	/**
	 * Recursively moves files from boilerplate folder to child theme
	 *
	 * @param $files:array
	 * @param $dir:string
	 *
	 * @return null
	 */
	private function moveFiles($files, $dir = null) {
		foreach($files as $file) {
			if(in_array($file, array('.', '..'))) continue;

			$extra = (($dir) ? ('/' . $dir) : '') . '/';
			$source = $this->paths['boilerplate'] . $extra . $file;
			$dest = $this->paths['child-dir'] . $extra . $file;

			if(is_dir($source)) {
				//create folder if needed
				if(!mkdir($dest)) Helpers::debug_log('Can\'t create dir in child theme: ' . $dest);
				
				//copy all the files in the folder
				$this->moveFiles(scandir($source), $dir . '/' . $file);
			} else {
				if(!copy($source, $dest)) { Helpers::debug_log('Can\'t create file in child theme: ' . $dest); }
			}
		}
	}

	/**
	 * Activates child theme
	 *
	 * @return null
	 */
	private function activateChildTheme() {
		$configPaths = Helpers::get_config('paths');
		$child = $configPaths['child-theme'];

		if(wp_get_theme($child)->exists()) {
			update_option('stylesheet', $child);
		}
	}
}