<?php

namespace PM;

if(class_exists('TimberSite')) {
	class TimberExtras extends \TimberSite {
		public function __construct() {
			parent::__construct();

			add_filter('timber_context', array($this, 'add_context'));
			add_action('save_post', array($this, 'refresh_cache'), 10, 3);

			$this->add_helpers_to_twig();
		}

		public function refresh_cache() {
			$loader = new \TimberLoader();
			$loader->clear_cache_timber();
			$loader->clear_cache_twig();
		}

		private function add_helpers_to_twig() {		
			$reflection = new \ReflectionClass(get_class($this));
			$methods = $reflection->getMethods(\ReflectionMethod::IS_FINAL);

			foreach($methods as $method) {
				$params = array();
				foreach($method->getParameters() as $param) {
					$params[] = ($param->getDefaultValue()) ?: '';
				}
				
				\TimberHelper::function_wrapper(array($this, $method->name), $params);
			}
		}

		public function add_context($context) {
			$context['options'] = get_fields('options');
			
			foreach(get_registered_nav_menus() as $k => $v) {
				/***************** DEPRECATED ***************/
				$context['menu_' . $k] = new \TimberMenu($k);
				/*************** END DEPRECATED *************/
				
				$context['menu'][$k] = new \TimberMenu($k);
			}

			return $context;
		}
		
		final public function breadcrumbs($before = '', $after = '') {
			if(function_exists('yoast_breadcrumb')) {
				return yoast_breadcrumb($before, $after, false);
			}
		}

		final public function pm_website_link($attr = null) {
		    return '<a href="' . (is_front_page()) ? 'http://www.purposemedia.co.uk' : '#' . '" ' . ($attr) ?: '' . '>Designed & Developed by Purpose Media</a>';
		}

		final public function get_template_link($name = '') {
			static $cache;

			if(!is_null($cache) && array_key_exists($name, $cache)) {
				return $cache[$name];
			}

			$post = get_posts(array(
				'post_type' => 'page',
				'posts_per_page' => 1,
				'meta_key' => '_wp_page_template',
				'meta_value' => $name . '.php'
			));

			if(!empty($post)) {
				$returnData = get_permalink($post[0]->ID);
				$cache[$name] = $returnData;

				return $returnData;
			}
		}
	}
}