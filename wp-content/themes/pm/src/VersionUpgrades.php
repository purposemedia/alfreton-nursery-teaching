<?php

namespace PM;

class VersionUpgrades {
	public function __construct($old, $new) {
		$this->old = filter_var($old, FILTER_SANITIZE_NUMBER_INT);
		$this->new = filter_var($new, FILTER_SANITIZE_NUMBER_INT);
	}

	public function run() {
		$reflection = new \ReflectionClass(get_class($this));
		$methods = $reflection->getMethods(\ReflectionMethod::IS_FINAL);

		$methods = array_map(function($obj) { return filter_var($obj->name, FILTER_SANITIZE_NUMBER_INT); }, $methods);
		
		foreach($methods as $name) {
			if($name > $this->old && $name <= $this->new) {
				$method = $reflection->getMethod('v' . $name);
				$method->setAccessible(true);
				$method->invoke($this);
			}
		}
	}
}